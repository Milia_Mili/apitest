import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { map, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http : HttpClient) { }


  public getEntries() : Observable<any> {
    return this.http.get<any>('https://api.publicapis.org/entries')
    .pipe(
      map(data=>{
        let arr = data.entries;
        return arr;
      }) 
    );
  }


}

